﻿using Microsoft.EntityFrameworkCore;
using WebSiteBanHang.DataAccess;
using WebSiteBanHang.Models;
namespace WebSiteBanHang.Repositories
{
    public class EFCategoryRepository : IcategoryRepository
    {
        private readonly ApplicationDbContext _context;
      /*  private List<Category> _categoryList;
        public IEnumerable<Category> GetAllCategories()
        {
            return _categoryList;
        }*/
        public EFCategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            // return await _context.Categorys.ToListAsync();
            return await _context.Categories.Include(p => p.Products) // Include thông tin về category
.ToListAsync();
        }
        public async Task<Category> GetByIdAsync(int id)
        {
            // return await _context.Categorys.FindAsync(id);
            // lấy thông tin kèm theo category
            return await _context.Categories.Include(p => p.Products).FirstOrDefaultAsync(p => p.Id == id);
        }
        public async Task AddAsync(Category Category)
        {
            _context.Categories.Add(Category);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(Category Category)
        {
            _context.Categories.Update(Category);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            var category = await _context.Categories.FindAsync(id);
            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();
        }
    }


}

